---
layout: handbook-page-toc
title: "GitLab Dedicated Architecture"
---

![GitLab Dedicated Group logo](./img/dedicated_team_logo.png)

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Architecture

This page collects a set of architectural documents and diagrams for GitLab Dedicated.

## High level overview

![High level overview diagram for GitLab Dedicated](./img/high-level-diagram.png)

## Tenant network

![Tenant network diagram for GitLab Dedicated](./img/tenant-network-diagram.png)

### AWS PrivateLink connection

![AWS PrivateLink diagram for GitLab Dedicated](./img/privatelink-diagram.png)

## Documents

- [From Dedicated to Cells: a Technical Analysis](from-dedicated-to-cells-technical-analysis.html)

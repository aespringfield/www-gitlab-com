---
title: "GitLab Patch Release: 16.5.2"
categories: releases
author: Steve Abrams
author_gitlab: sabrams
author_twitter: gitlab
description: "GitLab releases 16.5.2"
tags: patch releases, releases
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.5.2 for GitLab Community Edition and Enterprise Edition.

These versions resolve a number of regressions and bugs.

## GitLab Community Edition and Enterprise Edition

### 16.5.2

* [Backport to 16.5: Geo: Bring back legacy project Prometheus metrics](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135645)
* [Backport artifacts page breadcrumb fixes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135195)
* [Fix broken issue rendering when initial ID is null](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136065)
* [Backport - Create group wiki repo if absent when verifying on primary](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136243)
* [backport to 16.5: Fix Geo verification state backfill job can exceed batch size](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136399)
* [Fix assign security check permission checks](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136434)
* [Update postgres_exporter from 0.14.0 to 0.15.0 (16.5 backport)](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7228)

## Important notes on upgrading

This version does not include any new migrations, and for multi-node deployments, [should not require any downtime](https://docs.gitlab.com/ee/update/#upgrading-without-downtime).

Please be aware that by default the Omnibus packages will stop, run migrations,
and start again, no matter how “big” or “small” the upgrade is. This behavior
can be changed by adding a [`/etc/gitlab/skip-auto-reconfigure`](https://docs.gitlab.com/ee/update/zero_downtime.html) file,
which is only used for [updates](https://docs.gitlab.com/omnibus/update/README.html).

## Updating

To update, check out our [update page](/update/).

## GitLab subscriptions

Access to GitLab Premium and Ultimate features is granted by a paid [subscription](/pricing/).

Alternatively, [sign up for GitLab.com](https://gitlab.com/users/sign_in)
to use GitLab's own infrastructure.
